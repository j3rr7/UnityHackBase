﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace UnityHackBase
{
    public static GameObject Load;

    public static class Loader
    {
        public static void Init()
        {
            Loader.Load = new GameObject("Trainer Base");
            Loader.Load.transform.parent = null;
            Loader.Load.AddComponent<HackTrainer>();
            Object.DontDestroyOnLoad(Loader.Load);
        }
    }
}

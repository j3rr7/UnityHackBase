﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace UnityHackBase
{
    public class HackTrainer : MonoBehaviour
    {
        private Rect MainWindow;
        private bool MainWindowVisible = true;

        private void Start()
        {
            this.MainWindow = new Rect((float)(Screen.width / 2 - 100), (float)(Screen.height / 2 - 350), 250f, 50f);
        }

        private void Update()
        {
            if (Input.GetKeyDown(92))
            {
                this.MainWindowVisible = !this.MainWindowVisible;
            }
        }

        private void OnGUI()
        {
            if (this.MainWindowVisible && Event.current.type == 8)
            {
                GUI.backgroundColor = Color.black;
                GUIStyle guistyle = new GUIStyle(GUI.skin.window);
                guistyle.normal.textColor = Color.green;
                this.MainWindow = new Rect(this.MainWindow.x, this.MainWindow.y, 250f, 50f);
                this.MainWindow = GUILayout.Window(0, this.MainWindow, new GUI.WindowFunction(this.RenderUI), "Test Trainer v1", guistyle, new GUILayoutOption[0]);
            }
        }

        private void RenderUI(int id)
        {
            GUIStyle guistyle = new GUIStyle();
            guistyle.normal.textColor = Color.cyan;
            guistyle.alignment = 4;
            if (id == 0)
            {
                GUILayout.Label("Show/Hide: Backslash", guistyle, new GUILayoutOption[0]);
                GUILayout.Space(10f);
                GUI.color = Color.white;
                GUILayout.Button("Cheat 1", new GUILayoutOption[0]);
                GUILayout.Space(5f);
            }
            GUI.DragWindow();
        }
    }
}
